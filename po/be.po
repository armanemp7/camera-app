# Belarusian translation for camera-app
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the camera-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: camera-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-06-23 17:57+0000\n"
"PO-Revision-Date: 2020-11-26 15:47+0000\n"
"Last-Translator: Zmicer <nashtlumach@gmail.com>\n"
"Language-Team: Belarusian <https://translate.ubports.com/projects/ubports/"
"camera-app/be/>\n"
"Language: be\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<="
"4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 3.11.3\n"
"X-Launchpad-Export-Date: 2016-12-01 04:53+0000\n"

#: ../AdvancedOptions.qml:16
msgid "Settings"
msgstr "Налады"

#: ../AdvancedOptions.qml:48
msgid "Add date stamp on captured images"
msgstr "Дадаваць на выявы час фатаграфавання"

#. TRANSLATORS: this refers to the opacity  of date stamp added to captured images
#: ../AdvancedOptions.qml:70
msgid "Format"
msgstr "Фарматаваць"

#: ../AdvancedOptions.qml:122
msgid "Date formatting keywords"
msgstr "Ключавыя словы фарматавання даты"

#: ../AdvancedOptions.qml:127
msgid "the day as number without a leading zero (1 to 31)"
msgstr "дзень без нуля (ад 1 да 31)"

#: ../AdvancedOptions.qml:128
msgid "the day as number with a leading zero (01 to 31)"
msgstr "дзень з нулём (ад 01 да 31)"

#: ../AdvancedOptions.qml:129
msgid "the abbreviated localized day name (e.g. 'Mon' to 'Sun')."
msgstr "скарочаныя назвы дзён тыдня."

#: ../AdvancedOptions.qml:130
msgid "the long localized day name (e.g. 'Monday' to 'Sunday')."
msgstr "поўныя назвы дзён тыдня."

#: ../AdvancedOptions.qml:131
msgid "the month as number without a leading zero (1 to 12)"
msgstr "месяц без нуля (ад 1 да 12)"

#: ../AdvancedOptions.qml:132
msgid "the month as number with a leading zero (01 to 12)"
msgstr "месяц з нулём (ад 01 да 12)"

#: ../AdvancedOptions.qml:133
msgid "the abbreviated localized month name (e.g. 'Jan' to 'Dec')."
msgstr "скарочаныя назвы месяцаў."

#: ../AdvancedOptions.qml:134
msgid "the long localized month name (e.g. 'January' to 'December')."
msgstr "поўныя назвы месяцаў."

#: ../AdvancedOptions.qml:135
msgid "the year as two digit number (00 to 99)"
msgstr "год дзвюма лічбамі (ад 00 да 99)"

#: ../AdvancedOptions.qml:136
msgid ""
"the year as four digit number. If the year is negative, a minus sign is "
"prepended in addition."
msgstr "год чатырма лічбамі. Калі год адмоўны, у пачатку дадаецца мінус."

#: ../AdvancedOptions.qml:137
msgid "the hour without a leading zero (0 to 23 or 1 to 12 if AM/PM display)"
msgstr "гадзіны без нуля (ад 0 да 23; ад 1 да 12 )"

#: ../AdvancedOptions.qml:138
msgid "the hour with a leading zero (00 to 23 or 01 to 12 if AM/PM display)"
msgstr "гадзіны з нулём (ад 00 да 23; ад 01 да 12)"

#: ../AdvancedOptions.qml:139
msgid "the hour without a leading zero (0 to 23, even with AM/PM display)"
msgstr ""
"гадзіны без нуля (ад 0 да 23, нават з адзнакай \"да поўдня; пасля поўдня\")"

#: ../AdvancedOptions.qml:140
msgid "the hour with a leading zero (00 to 23, even with AM/PM display)"
msgstr ""
"гадзіны з нулём (ад 00 да 23, нават з адзнакай \"да поўдня; пасля поўдня\")"

#: ../AdvancedOptions.qml:141
msgid "the minute without a leading zero (0 to 59)"
msgstr "хвіліны без нуля (ад 0 да 59)"

#: ../AdvancedOptions.qml:142
msgid "the minute with a leading zero (00 to 59)"
msgstr "хвіліны з нулём (ад 00 да 59)"

#: ../AdvancedOptions.qml:143
msgid "the second without a leading zero (0 to 59)"
msgstr "секунды без нуля (ад 0 да 59)"

#: ../AdvancedOptions.qml:144
msgid "the second with a leading zero (00 to 59)"
msgstr "секунды з нулём (ад 00 да 59)"

#: ../AdvancedOptions.qml:145
msgid "the milliseconds without leading zeroes (0 to 999)"
msgstr "мілісекунды без нуля (ад 0 да 999)"

#: ../AdvancedOptions.qml:146
msgid "the milliseconds with leading zeroes (000 to 999)"
msgstr "мілісекунды з нулём (ад 000 да 999)"

#: ../AdvancedOptions.qml:147
msgid "use AM/PM display. AP will be replaced by either 'AM' or 'PM'."
msgstr "паказваць адзнаку \"да поўдня; пасля поўдня\"."

#: ../AdvancedOptions.qml:148
msgid "use am/pm display. ap will be replaced by either 'am' or 'pm'."
msgstr "паказваць адзнаку \"да поўдня; пасля поўдня\"."

#: ../AdvancedOptions.qml:149
msgid "the timezone (for example 'CEST')"
msgstr "часавы пояс (напрыклад, \"CEST\")"

#: ../AdvancedOptions.qml:158
msgid "Add to Format"
msgstr "Дадаць у фармат"

#. TRANSLATORS: this refers to the color of date stamp added to captured images
#: ../AdvancedOptions.qml:189
msgid "Color"
msgstr "Колер"

#. TRANSLATORS: this refers to the alignment of date stamp within captured images (bottom left, top right,etc..)
#: ../AdvancedOptions.qml:257
msgid "Alignment"
msgstr "Выраўноўванне"

#. TRANSLATORS: this refers to the opacity  of date stamp added to captured images
#: ../AdvancedOptions.qml:303
msgid "Opacity"
msgstr "Непразрыстасць"

#: ../DeleteDialog.qml:24
msgid "Delete media?"
msgstr "Выдаліць змесціва?"

#: ../DeleteDialog.qml:34 ../PhotogridView.qml:56 ../SlideshowView.qml:75
msgid "Delete"
msgstr "Выдаліць"

#: ../DeleteDialog.qml:40 ../ViewFinderOverlay.qml:1122
#: ../ViewFinderOverlay.qml:1136 ../ViewFinderOverlay.qml:1175
#: ../ViewFinderView.qml:494
msgid "Cancel"
msgstr "Скасаваць"

#: ../GalleryView.qml:200
msgid "No media available."
msgstr "Няма медыяфайлаў."

#: ../GalleryView.qml:235
msgid "Scanning for content..."
msgstr "Сканаванне..."

#: ../GalleryViewHeader.qml:82 ../SlideshowView.qml:45
msgid "Select"
msgstr "Абраць"

#: ../GalleryViewHeader.qml:83
msgid "Edit Photo"
msgstr "Рэдагаваць фота"

#: ../GalleryViewHeader.qml:83
msgid "Photo Roll"
msgstr "Фотастужка"

#: ../Information.qml:20
msgid "About"
msgstr "Звесткі"

#: ../Information.qml:82
msgid "Get the source"
msgstr "Атрымаць зыходны код"

#: ../Information.qml:83
msgid "Report issues"
msgstr "Паведаміць пра праблему"

#: ../Information.qml:84
msgid "Help translate"
msgstr "Дапамагчы з перакладам"

#: ../MediaInfoPopover.qml:25
msgid "Media Information"
msgstr "Звесткі"

#: ../MediaInfoPopover.qml:30
#, qt-format
msgid "Name : %1"
msgstr "Назва : %1"

#: ../MediaInfoPopover.qml:33
#, qt-format
msgid "Type : %1"
msgstr "Тып : %1"

#: ../MediaInfoPopover.qml:37
#, qt-format
msgid "Width : %1"
msgstr "Шырыня : %1"

#: ../MediaInfoPopover.qml:41
#, qt-format
msgid "Height : %1"
msgstr "Вышыня : %1"

#: ../NoSpaceHint.qml:33
msgid "No space left on device, free up space to continue."
msgstr "Не стае вольнага месца; вызваліце месца, каб працягнуць."

#: ../PhotoRollHint.qml:69
msgid "Swipe left for photo roll"
msgstr "Правядзіце ўлева, каб перайсці да фотастужкі"

#: ../PhotogridView.qml:42 ../SlideshowView.qml:60
msgid "Share"
msgstr "Падзяліцца"

#: ../PhotogridView.qml:69 ../SlideshowView.qml:53
msgid "Gallery"
msgstr "Галерэя"

#: ../SlideshowView.qml:68
msgid "Image Info"
msgstr "Звесткі пра выяву"

#: ../SlideshowView.qml:86
msgid "Edit"
msgstr "Рэдагаваць"

#: ../SlideshowView.qml:441
msgid "Back to Photo roll"
msgstr "Вярнуцца да фота"

#: ../UnableShareDialog.qml:25
msgid "Unable to share"
msgstr "Не атрымалася падзяліцца"

#: ../UnableShareDialog.qml:26
msgid "Unable to share photos and videos at the same time"
msgstr "Немагчыма адначасова падзяліцца фота і відэа"

#: ../UnableShareDialog.qml:30
msgid "Ok"
msgstr "Добра"

#: ../ViewFinderOverlay.qml:367 ../ViewFinderOverlay.qml:390
#: ../ViewFinderOverlay.qml:418 ../ViewFinderOverlay.qml:441
#: ../ViewFinderOverlay.qml:526 ../ViewFinderOverlay.qml:589
#: ../tests/unittests/tst_BottomEdgeIndicators.qml:42
#: ../tests/unittests/tst_BottomEdgeIndicators.qml:65
#: ../tests/unittests/tst_BottomEdgeIndicators.qml:86
msgid "On"
msgstr "Укл"

#: ../ViewFinderOverlay.qml:372 ../ViewFinderOverlay.qml:400
#: ../ViewFinderOverlay.qml:423 ../ViewFinderOverlay.qml:446
#: ../ViewFinderOverlay.qml:465 ../ViewFinderOverlay.qml:531
#: ../ViewFinderOverlay.qml:599
#: ../tests/unittests/tst_BottomEdgeIndicators.qml:47
#: ../tests/unittests/tst_BottomEdgeIndicators.qml:70
#: ../tests/unittests/tst_BottomEdgeIndicators.qml:91
msgid "Off"
msgstr "Выкл"

#: ../ViewFinderOverlay.qml:395
msgid "Auto"
msgstr "Аўта"

#: ../ViewFinderOverlay.qml:432
msgid "HDR"
msgstr "HDR"

#: ../ViewFinderOverlay.qml:470
msgid "5 seconds"
msgstr "5 секунд"

#: ../ViewFinderOverlay.qml:475
msgid "15 seconds"
msgstr "15 секунд"

#: ../ViewFinderOverlay.qml:493
msgid "Fine Quality"
msgstr "Высокая якасць"

#: ../ViewFinderOverlay.qml:498
msgid "High Quality"
msgstr "Высокая якасць"

#: ../ViewFinderOverlay.qml:503
msgid "Normal Quality"
msgstr "Звычайная якасць"

#: ../ViewFinderOverlay.qml:508
msgid "Basic Quality"
msgstr "Нізкая якасць"

#. TRANSLATORS: this will be displayed on an small button so for it to fit it should be less then 3 characters long.
#: ../ViewFinderOverlay.qml:541
msgid "SD"
msgstr "SD"

#: ../ViewFinderOverlay.qml:549
msgid "Save to SD Card"
msgstr "Захаваць на SD-картку"

#: ../ViewFinderOverlay.qml:554
msgid "Save internally"
msgstr "Захаваць на прыладзе"

#: ../ViewFinderOverlay.qml:594
msgid "Vibrate"
msgstr "Вібраваць"

#: ../ViewFinderOverlay.qml:1119
msgid "Low storage space"
msgstr "Не стае вольнага месца"

#: ../ViewFinderOverlay.qml:1120
msgid ""
"You are running out of storage space. To continue without interruptions, "
"free up storage space now."
msgstr "Сканчаецца вольнае месца. Каб пазбегнуць памылак, вызваліцца месца."

#: ../ViewFinderOverlay.qml:1133
msgid "External storage not writeable"
msgstr "Нельга запісваць на вонкавае сховішча"

#: ../ViewFinderOverlay.qml:1134
msgid ""
"It does not seem possible to write to your external storage media. Trying to "
"eject and insert it again might solve the issue, or you might need to format "
"it."
msgstr ""
"Немагчыма выканаць запіс на вонкавае сховішча. Паспрабуйце выняць яго і зноў "
"уставіць. Магчыма, давядзецца адфарматаваць носьбіт."

#: ../ViewFinderOverlay.qml:1172
msgid "Cannot access camera"
msgstr "Няма доступу да камеры"

#: ../ViewFinderOverlay.qml:1173
msgid ""
"Camera app doesn't have permission to access the camera hardware or another "
"error occurred.\n"
"\n"
"If granting permission does not resolve this problem, reboot your device."
msgstr ""
"Праграма камеры не можа атрымаць доступ да самой камеры, альбо адбылася "
"іншая памылка.\n"
"\n"
"Паспрабуйце даць праграме дазвол на доступ. Калі праблема не вырашылася, "
"перазапусціце прыладу."

#: ../ViewFinderOverlay.qml:1182
msgid "Edit Permissions"
msgstr "Змяніць правы доступу"

#: ../ViewFinderView.qml:485
msgid "Capture failed"
msgstr "Не атрымалася сфатаграфаваць"

#: ../ViewFinderView.qml:490
msgid ""
"Replacing your external media, formatting it, or restarting the device might "
"fix the problem."
msgstr ""
"Мажліва, праблему атрымаецца вырашыць заменай носьбіта, яго фарматаваннем ці "
"перазапускам прылады."

#: ../ViewFinderView.qml:491
msgid "Restarting your device might fix the problem."
msgstr "Мажліва, праблему атрымаецца вырашыць перазапускам прылады."

#: ../camera-app.qml:57
msgid "Flash"
msgstr "Выбліск"

#: ../camera-app.qml:58
msgid "Light;Dark"
msgstr "Дзень;Ноч"

#: ../camera-app.qml:61
msgid "Flip Camera"
msgstr "Павярнуць камеру"

#: ../camera-app.qml:62
msgid "Front Facing;Back Facing"
msgstr "Пярэдняя камера;Задняя камера"

#: ../camera-app.qml:65
msgid "Shutter"
msgstr "Засаўка"

#: ../camera-app.qml:66
msgid "Take a Photo;Snap;Record"
msgstr "Зрабіць фота;Імгненны здымак;Запіс"

#: ../camera-app.qml:69
msgid "Mode"
msgstr "Рэжым"

#: ../camera-app.qml:70
msgid "Stills;Video"
msgstr "Фота;Відэа"

#: ../camera-app.qml:74
msgid "White Balance"
msgstr "Баланс белага"

#: ../camera-app.qml:75
msgid "Lighting Condition;Day;Cloudy;Inside"
msgstr "Умовы асвятлення;Ясна;Пахмурна;Памяшканне"

#: ../camera-app.qml:380
#, qt-format
msgid "<b>%1</b> photos taken today"
msgstr "<b>%1</b> фотаздымкаў за сёння"

#: ../camera-app.qml:381
msgid "No photos taken today"
msgstr "Сёння не было фотаздымкаў"

#: ../camera-app.qml:391
#, qt-format
msgid "<b>%1</b> videos recorded today"
msgstr "<b>%1</b> відэазапісаў за сёння"

#: ../camera-app.qml:392
msgid "No videos recorded today"
msgstr "Сёння не было відэазапісаў"

#: camera-app.desktop.in.in.h:1
msgid "Camera"
msgstr "Камера"

#: camera-app.desktop.in.in.h:2
msgid "Camera application"
msgstr "Фотакамера"

#: camera-app.desktop.in.in.h:3
msgid "Photos;Videos;Capture;Shoot;Snapshot;Record"
msgstr "Фота;Відэа;Захоп;Здымка;Здымак;Запіс"
